//
//  ViewController.swift
//  GIAF
//
//  Created by GEEKUp on 6/2/16.
//  Copyright © 2016 GEEKUp. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let happyEmoji = Constants.Emojis.Happy
        print("Swift is fun! \(happyEmoji)")
        
        let segueIdentifier = SegueIdentifiers.Master.rawValue
        print(segueIdentifier)
        
        print(BlueCellIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

