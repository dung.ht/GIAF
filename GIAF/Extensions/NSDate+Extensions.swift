//
//  NSDate+Extensions.swift
//  wumbomessager
//
//  Created by GEEKUp on 6/2/16.
//  Copyright © 2016 GEEKUp. All rights reserved.
//

import Foundation

// DotNet: "/Date(1268123281843)/"
let DefaultFormat = "EEE MMM dd HH:mm:ss Z yyyy"
let RSSFormat = "EEE, d MMM yyyy HH:mm:ss ZZZ" // "Fri, 09 Sep 2011 15:26:08 +0200"
let AltRSSFormat = "d MMM yyyy HH:mm:ss ZZZ" // "09 Sep 2011 15:26:08 +0200"

public enum ISO8601Format: String {
    
    case Year = "yyyy" // 1997
    case YearMonth = "yyyy-MM" // 1997-07
    case Date = "yyyy-MM-dd" // 1997-07-16
    case DateTime = "yyyy-MM-dd'T'HH:mmZ" // 1997-07-16T19:20+01:00
    case DateTimeSec = "yyyy-MM-dd'T'HH:mm:ssZ" // 1997-07-16T19:20:30+01:00
    case DateTimeMilliSec = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // 1997-07-16T19:20:30.45+01:00
    
    init(dateString:String) {
        switch dateString.characters.count {
        case 4:
            self = ISO8601Format(rawValue: ISO8601Format.Year.rawValue)!
        case 7:
            self = ISO8601Format(rawValue: ISO8601Format.YearMonth.rawValue)!
        case 10:
            self = ISO8601Format(rawValue: ISO8601Format.Date.rawValue)!
        case 22:
            self = ISO8601Format(rawValue: ISO8601Format.DateTime.rawValue)!
        case 25:
            self = ISO8601Format(rawValue: ISO8601Format.DateTimeSec.rawValue)!
        default:// 28:
            self = ISO8601Format(rawValue: ISO8601Format.DateTimeMilliSec.rawValue)!
        }
    }
}

public enum DateFormat {
    case ISO8601(ISO8601Format?), DotNet, RSS, AltRSS, Custom(String)
}

public extension NSDate {
    
    // MARK: Intervals In Seconds
    private class func minuteInSeconds() -> Double { return 60 }
    private class func hourInSeconds() -> Double { return 3600 }
    private class func dayInSeconds() -> Double { return 86400 }
    private class func weekInSeconds() -> Double { return 604800 }
    private class func yearInSeconds() -> Double { return 31556926 }
    
    // MARK: Components
    private class func componentFlags() -> NSCalendarUnit { return [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.WeekOfYear, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second, NSCalendarUnit.Weekday, NSCalendarUnit.WeekdayOrdinal, NSCalendarUnit.WeekOfYear] }
    
    private class func components(fromDate fromDate: NSDate) -> NSDateComponents! {
        return NSCalendar.currentCalendar().components(NSDate.componentFlags(), fromDate: fromDate)
    }
    
    private func components() -> NSDateComponents  {
        return NSDate.components(fromDate: self)!
    }
    
    // MARK: Comparing Dates
    
    /**
    Returns true if dates are equal while ignoring time.
    
    - Parameter date: The Date to compare.
    */
    func isEqualToDateIgnoringTime(date: NSDate) -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: date)
        return ((comp1.year == comp2.year) && (comp1.month == comp2.month) && (comp1.day == comp2.day))
    }
    
    /**
     Returns Returns true if date is today.
     */
    func isToday() -> Bool
    {
        return self.isEqualToDateIgnoringTime(NSDate())
    }
    
    /**
     Returns true if date is tomorrow.
     */
    func isTomorrow() -> Bool
    {
        return self.isEqualToDateIgnoringTime(NSDate().dateByAddingDays(1))
    }
    
    /**
     Returns true if date is yesterday.
     */
    func isYesterday() -> Bool
    {
        return self.isEqualToDateIgnoringTime(NSDate().dateBySubtractingDays(1))
    }
    
    /**
     Returns true if date are in the same week.
     
     - Parameter date: The date to compare.
     */
    func isSameWeekAsDate(date: NSDate) -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: date)
        // Must be same week. 12/31 and 1/1 will both be week "1" if they are in the same week
        if comp1.weekOfYear != comp2.weekOfYear {
            return false
        }
        // Must have a time interval under 1 week
        return abs(self.timeIntervalSinceDate(date)) < NSDate.weekInSeconds()
    }
    
    /**
     Returns true if date is this week.
     */
    func isThisWeek() -> Bool
    {
        return self.isSameWeekAsDate(NSDate())
    }
    
    /**
     Returns true if date is next week.
     */
    func isNextWeek() -> Bool
    {
        let interval: NSTimeInterval = NSDate().timeIntervalSinceReferenceDate + NSDate.weekInSeconds()
        let date = NSDate(timeIntervalSinceReferenceDate: interval)
        return self.isSameWeekAsDate(date)
    }
    
    /**
     Returns true if date is last week.
     */
    func isLastWeek() -> Bool
    {
        let interval: NSTimeInterval = NSDate().timeIntervalSinceReferenceDate - NSDate.weekInSeconds()
        let date = NSDate(timeIntervalSinceReferenceDate: interval)
        return self.isSameWeekAsDate(date)
    }
    
    /**
     Returns true if dates are in the same year.
     
     - Parameter date: The date to compare.
     */
    func isSameYearAsDate(date: NSDate) -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: date)
        return (comp1.year == comp2.year)
    }
    
    /**
     Returns true if date is this year.
     */
    func isThisYear() -> Bool
    {
        return self.isSameYearAsDate(NSDate())
    }
    
    /**
     Returns true if date is next year.
     */
    func isNextYear() -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: NSDate())
        return (comp1.year == comp2.year + 1)
    }
    
    /**
     Returns true if date is last year.
     */
    func isLastYear() -> Bool
    {
        let comp1 = NSDate.components(fromDate: self)
        let comp2 = NSDate.components(fromDate: NSDate())
        return (comp1.year == comp2.year - 1)
    }
    
    /**
     Returns true if date is earlier than date.
     
     - Parameter date: The date to compare.
     */
    func isEarlierThanDate(date: NSDate) -> Bool
    {
        return self.earlierDate(date) == self
    }
    
    /**
     Returns true if date is later than date.
     
     - Parameter date: The date to compare.
     */
    func isLaterThanDate(date: NSDate) -> Bool
    {
        return self.laterDate(date) == self
    }
    
    /**
     Returns true if date is in future.
     */
    func isInFuture() -> Bool
    {
        return self.isLaterThanDate(NSDate())
    }
    
    /**
     Returns true if date is in past.
     */
    func isInPast() -> Bool
    {
        return self.isEarlierThanDate(NSDate())
    }
    
    
    // MARK: Adjusting Dates
    
    /**
    Creates a new date by a adding days.
    
    - Parameter days: The number of days to add.
    - Returns A new date object.
    */
    func dateByAddingDays(days: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.day = days
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by a substracting days.
     
     - Parameter days: The number of days to substract.
     - Returns A new date object.
     */
    func dateBySubtractingDays(days: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.day = (days * -1)
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by a adding hours.
     
     - Parameter days: The number of hours to add.
     - Returns A new date object.
     */
    func dateByAddingHours(hours: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.hour = hours
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by substracting hours.
     
     - Parameter days: The number of hours to substract.
     - Returns A new date object.
     */
    func dateBySubtractingHours(hours: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.hour = (hours * -1)
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by adding minutes.
     
     - Parameter days: The number of minutes to add.
     - Returns A new date object.
     */
    func dateByAddingMinutes(minutes: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.minute = minutes
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by substracting minutes.
     
     - Parameter days: The number of minutes to add.
     - Returns A new date object.
     */
    func dateBySubtractingMinutes(minutes: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.minute = (minutes * -1)
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by adding seconds.
     
     - Parameter seconds: The number of seconds to add.
     - Returns A new date object.
     */
    func dateByAddingSeconds(seconds: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.second = seconds
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date by substracting seconds.
     
     - Parameter days: The number of seconds to substract.
     - Returns A new date object.
     */
    func dateBySubtractingSeconds(seconds: Int) -> NSDate
    {
        let dateComp = NSDateComponents()
        dateComp.second = (seconds * -1)
        return NSCalendar.currentCalendar().dateByAddingComponents(dateComp, toDate: self, options: NSCalendarOptions(rawValue: 0))!
    }
    
    /**
     Creates a new date from the start of the day.
     
     - Returns A new date object.
     */
    func dateAtStartOfDay() -> NSDate
    {
        let components = self.components()
        components.hour = 0
        components.minute = 0
        components.second = 0
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    /**
     Creates a new date from the end of the day.
     
     - Returns A new date object.
     */
    func dateAtEndOfDay() -> NSDate
    {
        let components = self.components()
        components.hour = 23
        components.minute = 59
        components.second = 59
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    /**
     Creates a new date from the start of the week.
     
     - Returns A new date object.
     */
    func dateAtStartOfWeek() -> NSDate
    {
        let flags :NSCalendarUnit = [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.WeekOfYear, NSCalendarUnit.Weekday]
        let components = NSCalendar.currentCalendar().components(flags, fromDate: self)
        components.weekday = NSCalendar.currentCalendar().firstWeekday
        components.hour = 0
        components.minute = 0
        components.second = 0
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    /**
     Creates a new date from the end of the week.
     
     - Returns A new date object.
     */
    func dateAtEndOfWeek() -> NSDate
    {
        let flags :NSCalendarUnit = [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.WeekOfYear, NSCalendarUnit.Weekday]
        let components = NSCalendar.currentCalendar().components(flags, fromDate: self)
        components.weekday = NSCalendar.currentCalendar().firstWeekday + 6
        components.hour = 0
        components.minute = 0
        components.second = 0
        return NSCalendar.currentCalendar().dateFromComponents(components)!
    }
    
    /**
     Creates a new date from the first day of the month
     
     - Returns A new date object.
     */
    func dateAtTheStartOfMonth() -> NSDate
    {
        //Create the date components
        let components = self.components()
        components.day = 1
        //Builds the first day of the month
        let firstDayOfMonthDate :NSDate = NSCalendar.currentCalendar().dateFromComponents(components)!
        
        return firstDayOfMonthDate
        
    }
    
    /**
     Creates a new date from the last day of the month
     
     - Returns A new date object.
     */
    func dateAtTheEndOfMonth() -> NSDate {
        
        //Create the date components
        let components = self.components()
        //Set the last day of this month
        components.month += 1
        components.day = 0
        
        //Builds the first day of the month
        let lastDayOfMonth :NSDate = NSCalendar.currentCalendar().dateFromComponents(components)!
        
        return lastDayOfMonth
        
    }
    
    /**
     Creates a new date based on tomorrow.
     
     - Returns A new date object.
     */
    class func tomorrow() -> NSDate
    {
        return NSDate().dateByAddingDays(1).dateAtStartOfDay()
    }
    
    /**
     Creates a new date based on yesterdat.
     
     - Returns A new date object.
     */
    class func yesterday() -> NSDate
    {
        return NSDate().dateBySubtractingDays(1).dateAtStartOfDay()
    }
    
    
    // MARK: Retrieving Intervals
    
    /**
    Gets the number of seconds after a date.
    
    - Parameter date: the date to compare.
    - Returns The number of seconds
    */
    func secondsAfterDate(date: NSDate) -> Int
    {
        return Int(self.timeIntervalSinceDate(date))
    }
    
    /**
     Gets the number of seconds before a date.
     
     - Parameter date: The date to compare.
     - Returns The number of seconds
     */
    func secondsBeforeDate(date: NSDate) -> Int
    {
        return Int(date.timeIntervalSinceDate(self))
    }
    
    /**
     Gets the number of minutes after a date.
     
     - Parameter date: the date to compare.
     - Returns The number of minutes
     */
    func minutesAfterDate(date: NSDate) -> Int
    {
        let interval = self.timeIntervalSinceDate(date)
        return Int(interval / NSDate.minuteInSeconds())
    }
    
    /**
     Gets the number of minutes before a date.
     
     - Parameter date: The date to compare.
     - Returns The number of minutes
     */
    func minutesBeforeDate(date: NSDate) -> Int
    {
        let interval = date.timeIntervalSinceDate(self)
        return Int(interval / NSDate.minuteInSeconds())
    }
    
    /**
     Gets the number of hours after a date.
     
     - Parameter date: The date to compare.
     - Returns The number of hours
     */
    func hoursAfterDate(date: NSDate) -> Int
    {
        let interval = self.timeIntervalSinceDate(date)
        return Int(interval / NSDate.hourInSeconds())
    }
    
    /**
     Gets the number of hours before a date.
     
     - Parameter date: The date to compare.
     - Returns The number of hours
     */
    func hoursBeforeDate(date: NSDate) -> Int
    {
        let interval = date.timeIntervalSinceDate(self)
        return Int(interval / NSDate.hourInSeconds())
    }
    
    /**
     Gets the number of days after a date.
     
     - Parameter date: The date to compare.
     - Returns The number of days
     */
    func daysAfterDate(date: NSDate) -> Int
    {
        let interval = self.timeIntervalSinceDate(date)
        return Int(interval / NSDate.dayInSeconds())
    }
    
    /**
     Gets the number of days before a date.
     
     - Parameter date: The date to compare.
     - Returns The number of days
     */
    func daysBeforeDate(date: NSDate) -> Int
    {
        let interval = date.timeIntervalSinceDate(self)
        return Int(interval / NSDate.dayInSeconds())
    }
    
    
    // MARK: Decomposing Dates
    
    /**
    Returns the nearest hour.
    */
    func nearestHour () -> Int {
        let halfHour = NSDate.minuteInSeconds() * 30
        var interval = self.timeIntervalSinceReferenceDate
        if  self.seconds() < 30 {
            interval -= halfHour
        } else {
            interval += halfHour
        }
        let date = NSDate(timeIntervalSinceReferenceDate: interval)
        return date.hour()
    }
    /**
     Returns the year component.
     */
    func year () -> Int { return self.components().year  }
    /**
     Returns the month component.
     */
    func month () -> Int { return self.components().month }
    /**
     Returns the week of year component.
     */
    func week () -> Int { return self.components().weekOfYear }
    /**
     Returns the day component.
     */
    func day () -> Int { return self.components().day }
    /**
     Returns the hour component.
     */
    func hour () -> Int { return self.components().hour }
    /**
     Returns the minute component.
     */
    func minute () -> Int { return self.components().minute }
    /**
     Returns the seconds component.
     */
    func seconds () -> Int { return self.components().second }
    /**
     Returns the weekday component.
     */
    func weekday () -> Int { return self.components().weekday }
    /**
     Returns the nth days component. e.g. 2nd Tuesday of the month is 2.
     */
    func nthWeekday () -> Int { return self.components().weekdayOrdinal }
    /**
     Returns the days of the month.
     */
    func monthDays () -> Int { return NSCalendar.currentCalendar().rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: self).length }
    /**
     Returns the first day of the week.
     */
    func firstDayOfWeek () -> Int {
        let distanceToStartOfWeek = NSDate.dayInSeconds() * Double(self.components().weekday - 1)
        let interval: NSTimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek
        return NSDate(timeIntervalSinceReferenceDate: interval).day()
    }
    /**
     Returns the last day of the week.
     */
    func lastDayOfWeek () -> Int {
        let distanceToStartOfWeek = NSDate.dayInSeconds() * Double(self.components().weekday - 1)
        let distanceToEndOfWeek = NSDate.dayInSeconds() * Double(7)
        let interval: NSTimeInterval = self.timeIntervalSinceReferenceDate - distanceToStartOfWeek + distanceToEndOfWeek
        return NSDate(timeIntervalSinceReferenceDate: interval).day()
    }
    /**
     Returns true if a weekday.
     */
    func isWeekday() -> Bool {
        return !self.isWeekend()
    }
    /**
     Returns true if weekend.
     */
    func isWeekend() -> Bool {
        let range = NSCalendar.currentCalendar().maximumRangeOfUnit(NSCalendarUnit.Weekday)
        return (self.weekday() == range.location || self.weekday() == range.length)
    }

    // MARK: Static Cached Formatters
    
    /**
    Returns a cached static array of NSDateFormatters so that thy are only created once.
    */
    private class func sharedDateFormatters() -> [String: NSDateFormatter] {
        struct Static {
            static var formatters: [String: NSDateFormatter]? = nil
            static var once: dispatch_once_t = 0
        }
        dispatch_once(&Static.once) {
            Static.formatters = [String: NSDateFormatter]()
        }
        return Static.formatters!
    }
    
}