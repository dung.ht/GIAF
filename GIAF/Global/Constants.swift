//
//  Constants.swift
//  GIAF
//
//  Created by GEEKUp on 6/2/16.
//  Copyright © 2016 GEEKUp. All rights reserved.
//

import Foundation
import UIKit

class Constants: NSObject {

    //MARK: create constant by struct
    struct Emojis {
        static let Happy = "😄"
        static let Sad = "😢"
    }
    
}

//MARK: create constant by enum
enum SegueIdentifiers: String {
    case Master = "MasterViewController"
    case Detail = "DetailViewController"
}


// MARK: create constant by let
let BlueCellIdentifier = "BlueCellIdentifier"
let LargeCellIdentifier = "LargeCellIdentifier"