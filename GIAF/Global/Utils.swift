//
//  Utils.swift
//  GIAF
//
//  Created by Huynh Tri Dung on 6/2/16.
//  Copyright © 2016 Huynh Tri Dung. All rights reserved.
//

import UIKit
import Foundation

class Utils: UIView {
    class func widthCell ()-> CGFloat {
        let model = UIDevice().modelName
        if ["iPhone 5","iPhone 5s","iPhone 5c","iPhone 5se","iPhone 5SE"].contains(model){
            return 185
        }else if ["iPhone 6", "iPhone 6S"].contains(model) {
            return 220
        }else if ["iPhone 6 Plus", "iPhone 6s Plus"].contains(model){
            return 365
        }else {
            let screenSize = UIScreen.mainScreen().bounds.size
            return screenSize.width - 146
        }
    }
    
    class func widthMyCell ()-> CGFloat{
        let model = UIDevice().modelName
        if ["iPhone 5","iPhone 5s","iPhone 5c","iPhone 5se","iPhone 5SE"].contains(model){
            return 220
        }else if ["iPhone 6", "iPhone 6S"].contains(model) {
            return 255
        }else if ["iPhone 6 Plus", "iPhone 6s Plus"].contains(model){
            return 420
        }else {
            let screenSize = UIScreen.mainScreen().bounds.size
            return screenSize.width - 110
        }
    }
    
    //MARK: Font method
    class func defautFont_Regular(fontsize:CGFloat) ->UIFont{
        return UIFont (name: "Lato-Regular", size: fontsize)!
    }
    
    //MARK: Color methods
    class func colorFromRGB(rgbValue: UInt, alphaValue: CGFloat) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alphaValue
        )
    }
    
    class func colorFromRedValue(redValue:Int, greenValue:Int, blueValue:Int, alphaValue: CGFloat) -> UIColor {
        return UIColor (
            red: CGFloat(redValue)/255,
            green: CGFloat(greenValue)/255,
            blue: CGFloat(blueValue)/255,
            alpha: alphaValue
        )
    }
    
    //MARK: dateTime
    class func simpleDateTimeStampFromString(dateTime:NSDate)->String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "ITC")
        let timeStamp = dateFormatter.stringFromDate(dateTime)
        return timeStamp
    }
    
    class func simpleTimeStampFromString(dateTime:NSDate)->String {
        switch UIDevice.currentDevice().systemVersion.compare("9.0.0", options: NSStringCompareOptions.NumericSearch) {
        case .OrderedSame, .OrderedDescending:
            //            println("iOS >= 9.0")
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            dateFormatter.timeZone = NSTimeZone(name: "ITC")
            let timeStamp = dateFormatter.stringFromDate(dateTime)
            return timeStamp.lowercaseString
        case .OrderedAscending:
            //            println("iOS < 9.0")
            return timeInDayForDate(dateTime)
        }
    }
    
    class func timeInDayForDate(date:NSDate) ->String {
        let calendar = NSCalendar.currentCalendar()
        let unitFlags: NSCalendarUnit = [.Hour, .Day, .Minute]
        let components:NSDateComponents = calendar.components(unitFlags, fromDate: date)
        var timeSpace = "", timeStr = ""
        if (components.hour > 12) {
            timeSpace = "pm"
        }else {
            timeSpace = "am"
        }
        //        if (components.minute > 0) {
        timeStr = timeStr.stringByAppendingFormat("%ld:%.2ld %@", components.hour%12,components.minute,timeSpace)
        //        }else {
        //             timeStr = timeStr.stringByAppendingFormat("%d %@", components.hour%12, timeSpace)
        //        }
        return timeStr
    }
    
    class func showDateTimeStamp(date:NSDate)->String {
        if date.isToday() {
            return Utils.simpleTimeStampFromString(date)
        } else  if date.isYesterday(){
            return "Yesterday"
        } else {
            return "\(Utils.simpleDateTimeStampFromString(date))"
        }
    }
    
    class func trim(inputString:String) -> String {
        return inputString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
}
